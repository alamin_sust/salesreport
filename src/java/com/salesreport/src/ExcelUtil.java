/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.salesreport.src;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;

/**
 *
 * @author md_al
 */
public class ExcelUtil {
    
    public static List<ExcelRow> getExcelContent(int userId) {
        FileInputStream fis = null;
        List<ExcelRow>excelRowList = new LinkedList<ExcelRow>();
        try {
            String filePath = Statics.FILE_PATH_PREFIX +userId+".xlsx" ;

            fis = new FileInputStream(filePath);

 

            // Using XSSF for xlsx format, for xls use HSSF

            Workbook workbook = new XSSFWorkbook(fis);
            DataFormatter objDefaultFormat = new DataFormatter();
            FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);

            int numberOfSheets = workbook.getNumberOfSheets();

            
 
            //looping over each workbook sheet
            

            for (int i = 0; i < numberOfSheets; i++) {
                int rowNum=0;
                Sheet sheet = workbook.getSheetAt(i);

                Iterator rowIterator = sheet.iterator();

                //iterating over each row

                while (rowIterator.hasNext()) {
                    if(rowNum==0) {
                      rowIterator.next();
                      rowNum++;
                      continue;
                    }
                    ExcelRow excelRow = new ExcelRow(userId,rowNum);
                    Row row = (Row) rowIterator.next();

                    Iterator cellIterator = row.cellIterator();

                    //Iterating over each cell (column wise)  in a particular row.

                    
                    while (cellIterator.hasNext()) {

 

                        Cell cell = (Cell) cellIterator.next();

                        //The Cell Containing String will is name.

                        
                        objFormulaEvaluator.evaluate(cell); // This will evaluate the cell, And any type of cell will return string value
                        String cellValueStr = objDefaultFormat.formatCellValue(cell,objFormulaEvaluator);


                        
                        //if (CellType.STRING == cell.getCellType()) {
                            excelRow.addToColList(cellValueStr);
                        //} else if (CellType.NUMERIC == cell.getCellType()) {
                        //    cell.setCellType(CellType.STRING);
                        //    excelRow.addToColList(cell.getStringCellValue());
                        //}

                    }

                    //end iterating a row, add all the elements of a row in list

                    excelRowList.add(excelRow);
                    rowNum++;
                }

            }

 

            fis.close();

 

        } catch (FileNotFoundException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        return excelRowList;
    }
    
    public static boolean writeExcelContent(int userId, List<ExcelRow> excelRowList) throws FileNotFoundException, IOException {
        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat, 
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Report");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        List<String> excelHeaderList = Statics.getExcelHeaderList();
        // Create cells
        for(int i = 0; i < excelHeaderList.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(excelHeaderList.get(i));
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        //CellStyle dateCellStyle = workbook.createCellStyle();
        //dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with employees data
        int rowNum = 1;
        for(ExcelRow excelRow: excelRowList) {
            Row row = sheet.createRow(rowNum++);

            List<String>colList = excelRow.getColList();
            for (int i=0; i<colList.size(); i++) {
                row.createCell(i)
                    .setCellValue(colList.get(i));
            }
            

            //Cell dateOfBirthCell = row.createCell(2);
            //dateOfBirthCell.setCellValue(employee.getDateOfBirth());
            //dateOfBirthCell.setCellStyle(dateCellStyle);

        }

		// Resize all columns to fit the content size
        for(int i = 0; i < excelHeaderList.size(); i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        String filePath = Statics.FILE_PATH_PREFIX +userId+"f.xlsx" ;
        FileOutputStream fileOut = new FileOutputStream(filePath);
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        workbook.close();

        return true;
    }
}
