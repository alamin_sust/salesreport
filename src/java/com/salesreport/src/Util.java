/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.salesreport.src;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author md_al
 */
public class Util {
    
    // mm/dd/yyyy to yyyy-mm-dd
    public static String formatDate(String date) {
        return date.substring(6)+"-"+date.substring(0,2)+"-"+date.substring(3,5);
    }
    
    public static List<ExcelRow> getFilteredExcelRowsByDate(List<ExcelRow> rows, String dateFrom, String dateTo) {
        List<ExcelRow> ret = new LinkedList<ExcelRow>();
        for(int i=0;i<rows.size();i++) {
            ExcelRow row = rows.get(i);
            if(Util.formatDate(row.getColList().get(0)).compareTo(dateFrom)>=0
                    &&Util.formatDate(row.getColList().get(0)).compareTo(dateTo)<=0) {
                ret.add(row);
            }
        }
        return Util.getSortedExcelRows(ret, 0);
    }

    public static List<ExcelRow> getSortedExcelRows(List<ExcelRow> rows, int sortBy) {
        List<ExcelRow> ret = rows;

        boolean isHeaderNumeric = Statics.isHeaderNumeric(sortBy);
        
        Collections.sort(ret, new Comparator<ExcelRow>() {

            public int compare(ExcelRow o1, ExcelRow o2) {
                // compare two instance of `Score` and return `int` as result.
                if(isHeaderNumeric) {
                    return Double.parseDouble(o2.getColList().get(sortBy))<(Double.parseDouble(o1.getColList().get(sortBy)))?-1:1;
                } else {
                    return o2.getColList().get(sortBy).compareTo(o1.getColList().get(sortBy));
                }
            }
        });

        return ret;
    }
}
