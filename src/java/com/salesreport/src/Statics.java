/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.salesreport.src;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author md_al
 */
public class Statics {
    
    public static String EXCEL_HEADERS[] = {"ShippedDate","Channel","OrderNumber","Vendor","SKU","Product","Quantity","TotalPrice","LandedCost","TotalPriceafterTerms","efactoryFreight","Margin","Vendor 35% Margin"};
    public static boolean EXCEL_HEADERS_IS_NUMERIC[] = {false,false,false,false,false,false,true,true,true,true,true,true,true};
    
    public static final String FILE_PATH_PREFIX = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\SalesReport\\web\\files\\";
    
    public static List<String> getExcelHeaderList() {
        return Arrays.asList(EXCEL_HEADERS);
    }
    
    public static boolean isHeaderNumeric(int index) {
        return EXCEL_HEADERS_IS_NUMERIC[index];
    }
} 
