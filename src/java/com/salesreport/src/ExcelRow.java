/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.salesreport.src;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author md_al
 */
public class ExcelRow {
    private int id;
    private int rowNum;
    private List<String> colList = new LinkedList<String>();
    
    public ExcelRow(){}
    public ExcelRow(int id, int rowNum) {
        this.id = id;
        this.rowNum = rowNum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public List<String> getColList() {
        return colList;
    }

    public void setColList(List<String> colList) {
        this.colList = colList;
    }

    public void addToColList(String value) {
        colList.add(value);
    }
    
    
}
