<%@page import="java.util.TreeMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.salesreport.src.Util"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.ss.usermodel.Cell"%>
<%@page import="org.apache.poi.ss.usermodel.Row"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.poi.ss.usermodel.Sheet"%>
<%@page import="org.apache.poi.xssf.usermodel.XSSFWorkbook"%>
<%@page import="org.apache.poi.ss.usermodel.Workbook"%>
<%@page import="java.util.LinkedList"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="com.salesreport.connection.Database"%>
<%@page import="com.salesreport.src.ExcelUtil"%>
<%@page import="com.salesreport.src.ExcelRow"%>
<%@page import="java.util.List"%>
<%@page import="com.salesreport.src.Statics"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sales Report</title>

        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="vendor/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        


    </head>

    <body onload="chartInit();">
        <%
            if (session.getAttribute("username") == null) {
                response.sendRedirect("login.jsp");
            }

            Database db = new Database();
            db.connect();
            int userId = Integer.parseInt(session.getAttribute("userId").toString());
            List<ExcelRow> excelRowList = ExcelUtil.getExcelContent(userId);; 
            try {
                List<String> headerList = Statics.getExcelHeaderList();
                
                
                if(request.getParameter("sortBy")!=null) {
                    excelRowList = Util.getSortedExcelRows(excelRowList, Integer.parseInt(request.getParameter("sortBy")));
                }
                
                if(request.getParameter("dateFrom")!=null&&!request.getParameter("dateFrom").equals("")
                        &&request.getParameter("dateTo")!=null&&!request.getParameter("dateTo").equals("")) {
                    excelRowList = Util.getFilteredExcelRowsByDate(excelRowList, request.getParameter("dateFrom"),request.getParameter("dateTo"));
                }
                
                
                
                boolean isWriteSuccessfull = ExcelUtil.writeExcelContent(userId, excelRowList);

                
                


        %>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">Sales Report</a>
                </div>
                <!-- /.navbar-header -->

                

                <div class="nav navbar-top-links navbar-right" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="home.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <%if(session.getAttribute("username").equals("admin")){%>
                            <li>
                                <a href="vendors.jsp"><i class="fa fa-user fa-fw"></i> Vendors</a>
                            </li>
                            <%}%>
                            <li>
                                <a href="login.jsp?logout=true"><i class="fa fa-user fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="">
                
                <%--
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-comments fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">26</div>
                                        <div>New Comments!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">12</div>
                                        <div>New Tasks!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">124</div>
                                        <div>New Orders!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">13</div>
                                        <div>Support Tickets!</div>
                                    </div>
                                </div>
                            </div>
                            <a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                --%>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="fa fa-bar-chart-o fa-fw"></i> Date vs. Total Sales Chart
                                <%--<div class="pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            Actions
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a href="#">Action</a>
                                            </li>
                                            <li><a href="#">Another action</a>
                                            </li>
                                            <li><a href="#">Something else here</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                --%>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div id="morris-area-chart"></div>
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->

                    </div>
                    <!-- /.col-lg-8 -->

                </div>
                <!-- /.row -->

                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-default">
<<<<<<< HEAD
                            <div class="panel-heading"  style="padding-bottom: 70px;">
=======
                            <div class="panel-heading"  style="padding-bottom: 40px;">
>>>>>>> 162f6bc9a59cc680fba28f2f63d327103278a3a0
                                <div><b>Report</b></div>
                                <div class="pull-left">
                                    <form method="get" action="home.jsp">Filter by date: From&nbsp;<input value='<%=request.getParameter("dateFrom")%>' type="date" name="dateFrom" required="">&nbsp;To&nbsp;<input value='<%=request.getParameter("dateTo")%>' type="date" name="dateTo" required="">&nbsp;<button class="btn btn-warning" type="submit">Filter</button></form>
                                </div>    
                                <div class="pull-right">
                                    <a href="downloadExcel.jsp?userId=<%=session.getAttribute("userId")%>"><span class="badge">download excel</span></a>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            Sort By
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <%for(int i=0;i<headerList.size();i++) {%>
                                            <li><a href="home.jsp?sortBy=<%=i%>"><%=headerList.get(i)%></a>
                                                
                                            </li>
                                            <%}%>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
                            <!-- /.panel-heading -->
                            <div class="panel-body table table-responsive">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <%for(int i=0;i<headerList.size();i++) {%>
                                            <th><%=headerList.get(i)%>
                                            <%if(request.getParameter("sortBy")!=null&&Integer.parseInt(request.getParameter("sortBy"))==i){%>
                                                <i class="fa fa-arrow-down"></i>
                                                <%}%>
                                            </th>
                                                <%}%>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%for (ExcelRow excelRow : excelRowList) {
                                                List<String> colList = excelRow.getColList();
                                        %>
                                        <tr class="odd gradeX">
                                            <%
                                                for (String value : colList) {
                                            %>
                                            <td class="center"><%=value%></td>
                                            <%}%>
                                        </tr>
                                        <%}%>
                                    </tbody>
                                </table>

                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>


            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="vendor/metisMenu/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="vendor/raphael/raphael.min.js"></script>
        <script src="vendor/morrisjs/morris.min.js"></script>
        <script src="data/morris-data.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>
        <%
            } catch (Exception e) {
                System.out.println(e);
            } finally {

                db.close();
            }
        %>
    </body>
    <script>
        Morris.Area({
        element: 'morris-area-chart',
        
        data: [
        <%excelRowList = Util.getSortedExcelRows(excelRowList, 7);
            Map<String,String>dateVsTotalSalesMap = new TreeMap<>();
            for(int i=0;i<excelRowList.size();i++){
                String date = Util.formatDate(excelRowList.get(i).getColList().get(0));
                if(dateVsTotalSalesMap.containsKey(date)) {
                    String totalSales = String.valueOf(Double.parseDouble(dateVsTotalSalesMap.get(date))
                            +Double.parseDouble(excelRowList.get(i).getColList().get(7)));
                    dateVsTotalSalesMap.put(date, totalSales);
                } else {
                    dateVsTotalSalesMap.put(date, excelRowList.get(i).getColList().get(7));
                }
            }
            int i=0;
            for(Map.Entry entry:dateVsTotalSalesMap.entrySet()){
                System.out.println("date: "+entry.getKey()+", totalPrice: "+entry.getValue());
        if(i++>0){%>,<%}%>    
        {
            period: '<%=entry.getKey()%>',
            totalPrice: <%=entry.getValue()%>
        }
        <%}%>
        ],
        xkey: 'period',
        ykeys: ['totalPrice'],
        labels: ['Total Sales'],
        pointSize: 3,
        hideHover: 'auto',
        resize: true
    });
        </script>
</html>
