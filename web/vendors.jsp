<!DOCTYPE html>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.File"%>
<%@page import="java.io.InputStream"%>
<%@page import="com.salesreport.connection.Database"%>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sales Report</title>

        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="vendor/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->



    </head>

    <body>
        <%
            if (session.getAttribute("username") == null || !session.getAttribute("username").equals("admin")) {
                response.sendRedirect("home.jsp");
            }

            Database db = new Database();
            db.connect();

            session.setAttribute("sM", null);
            session.setAttribute("eM", null);

            try {
            if (request.getParameter("insertOrUpdate") != null) {
                Statement st2 = db.connection.createStatement();
                Statement st3 = db.connection.createStatement();
                if (request.getParameter("userId").equals("-1")) {
                    String q3 = "select max(id)+1 as mx from user";
                    ResultSet rs3 = st3.executeQuery(q3);
                    rs3.next();

                    String q2 = "insert into user (id,username,password,sku) values(" + rs3.getString("mx")
                            + ",'" + request.getParameter("username")
                            + "','" + request.getParameter("password")
                            + "','" + request.getParameter("sku")
                            + "')";
                    st2.executeUpdate(q2);
                    session.setAttribute("sM", "Inserted " + request.getParameter("username") + " Successfully");
                } else {
                    String q2 = "update user set username='" + request.getParameter("username")
                            + "',password='" + request.getParameter("password")
                            + "',sku='" + request.getParameter("sku")
                            + "' where id=" + request.getParameter("userId");
                    st2.executeUpdate(q2);
                    session.setAttribute("sM", "Updated Successfully");
                }

            }

            Statement st = db.connection.createStatement();
            String q = "select * from user where id>0 and username!='admin'";
            ResultSet rs = st.executeQuery(q);

        %>

        <%                  //file start
            String saveFile = new String();
            String saveFile2 = new String();
            String contentType = request.getContentType();
            if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
                DataInputStream in = new DataInputStream(request.getInputStream());

                int formDataLength = request.getContentLength();
                byte dataBytes[] = new byte[formDataLength];
                int byteRead = 0;
                int totalBytesRead = 0;

                while (totalBytesRead < formDataLength) {
                    byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
                    totalBytesRead += byteRead;
                }
                String file = new String(dataBytes);

                saveFile = file.substring(file.indexOf("filename=\"") + 10);
                saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
                saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));

                int lastIndex = contentType.lastIndexOf("=");

                String boundary = contentType.substring(lastIndex + 1, contentType.length());

                int pos;

                pos = file.indexOf("filename=\"");
                pos = file.indexOf("\n", pos) + 1;
                pos = file.indexOf("\n", pos) + 1;
                pos = file.indexOf("\n", pos) + 1;

                int boundaryLocation = file.indexOf(boundary, pos) - 4;

                int startPos = ((file.substring(0, pos)).getBytes()).length;
                int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;

                saveFile = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\SalesReport\\build\\web\\files\\" + request.getParameter("userId") + ".xlsx";
                saveFile2 = "C:\\Users\\md_al\\Documents\\NetBeansProjects\\SalesReport\\web\\files\\" + request.getParameter("userId") + ".xlsx";
                //saveFile = "C:/uploadDir2/" + saveFile;
                //out.print(saveFile);
                File ff = new File(saveFile);
                File ff2 = new File(saveFile2);

                try {
                    FileOutputStream fileOut = new FileOutputStream(ff);
                    fileOut.write(dataBytes, startPos, (endPos - startPos));
                    fileOut.flush();
                    fileOut.close();
                    FileOutputStream fileOut2 = new FileOutputStream(ff2);
                    fileOut2.write(dataBytes, startPos, (endPos - startPos));
                    fileOut2.flush();
                    fileOut2.close();
                    session.setAttribute("sM", "Uploaded Successfully");
                } catch (Exception e) {
                    out.println(e);
                }

            }
        %>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp">Sales Report</a>
                </div>
                <!-- /.navbar-header -->

                

                <div class="nav navbar-top-links navbar-right" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>
                                <a href="home.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <%if(session.getAttribute("username").equals("admin")){%>
                            <li>
                                <a href="vendors.jsp"><i class="fa fa-user fa-fw"></i> Vendors</a>
                            </li>
                            <%}%>
                            <li>
                                <a href="login.jsp?logout=true"><i class="fa fa-user fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="row">
                    <%if (session.getAttribute("sM") != null) {%>
                        <div class="alert alert-success"><%=session.getAttribute("sM")%></div>
                        <%}%>
                        <%if (session.getAttribute("eM") != null) {%>
                        <div class="alert alert-danger"><%=session.getAttribute("eM")%></div>
                        <%}%>
                    <div class="col-lg-12">
                        <h1 class="page-header">Admin Panel</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>



                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                All Vendors
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body table-responsive">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Password</th>
                                            <%--<th>SKU</th>--%>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="odd gradeX">
                                    <form method="POST" action="vendors.jsp" class="form-group">
                                        <td><input  type="text" name="username" placeholder="username" required=""/></td>
                                        <td><input type="password" name="password" placeholder="password" required=""/></td>
                                        <%--<td class="center"><input type="text" name="sku" placeholder="SKU" required=""/></td>--%>
                                        <td class="center">
                                            <select name="userId" required="">
                                                <option value="-1">--Insert New--</option>
                                                <%while (rs.next()) {%>
                                                <option value="<%=rs.getString("id")%>">Update <%=rs.getString("username")%></option>    
                                                <%}
                                                    rs.beforeFirst();
                                                %>
                                            </select>
                                        </td>
                                        <td class="center">
                                            <input type="hidden" name="insertOrUpdate" value="true"/> 
                                            <button class="btn btn-success" type="submit">Submit</button>
                                        </td>
                                    </form>
                                    </tr>
                                    <%while (rs.next()) {%>
                                    <tr class="odd gradeX">
                                        <td><%=rs.getString("username")%></td>
                                        <td><%=rs.getString("password")%></td>
                                        <%--<td class="center"><%=rs.getString("sku")%></td>--%>
                                    <form method="POST" action="vendors.jsp?userId=<%=rs.getString("id")%>" enctype="multipart/form-data">
                                        <td class="center">

                                            <input type="file" name="file" value="" required=""/>

                                        </td>
                                        <td class="center">
                                            <input type="submit" value="UPLOAD" name="submit" class="btn btn-primary"/>
                                        </td>      
                                    </form>
                                    </tr>
                                    <%}%>
                                    </tbody>
                                </table>

                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>


            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="vendor/metisMenu/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="vendor/raphael/raphael.min.js"></script>
        <script src="vendor/morrisjs/morris.min.js"></script>
        <script src="data/morris-data.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>
        <%} catch (Exception e) {
                System.out.println(e);
            } finally {
                db.close();
            } 
        %>
    </body>

</html>
