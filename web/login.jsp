<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="com.salesreport.connection.Database"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Sales Report</title>

        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body background="dist/img/login-bg.jpg">

        <%
            Database db = new Database();
            db.connect();

            session.setAttribute("sM", null);
            session.setAttribute("eM", null);
            //try {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String sku = request.getParameter("sku");

            if (request.getParameter("logout") != null) {
                session.setAttribute("username", null);
                session.setAttribute("sM", "Successfully Logged Out!");
            }

            if (request.getParameter("login") != null) {
                Statement st = db.connection.createStatement();
                String q = "select * from user where username='"
                        + request.getParameter("username") + "'";
                ResultSet rs = st.executeQuery(q);
                if (rs.next()) {
                    if (rs.getString("password").equals(password)) {
                        session.setAttribute("userId", rs.getString("id"));
                        session.setAttribute("username", rs.getString("username"));
                        session.setAttribute("sM", "Successfully Logged In!");
                        response.sendRedirect("home.jsp");
                    } else {
                        session.setAttribute("eM", "Password Doesn't Match!");
                    }
                } else {
                    session.setAttribute("eM", "Username Doesn't Exist!");
                }
            }

            if (request.getParameter("register") != null) {
                Statement st0 = db.connection.createStatement();
                String q0 = "select max(id)+1 as mxId from user";
                ResultSet rs0 = st0.executeQuery(q0);
                rs0.next();
                String mxId = rs0.getString("mxId");

                Statement st2 = db.connection.createStatement();
                String q2 = "select count(*) cnt from user where username='"
                        + request.getParameter("username") + "'";
                ResultSet rs2 = st2.executeQuery(q2);
                rs2.next();
                if (rs2.getString("cnt").equals("0")) {
                    Statement st = db.connection.createStatement();
                    String q = "insert into user (id,username,password,sku) values("
                            + mxId + ",'" + username + "','" + password + "','" + sku + "')";
                    st.executeUpdate(q);
                    session.setAttribute("sM", "Successfully Registered!");
                } else {
                    session.setAttribute("eM", "Username Already Exists!");
                }
            }

        %>

        <div class="container">
            <div class="row">
            <%if (session.getAttribute("sM") != null) {%>
            <div class="alert alert-success"><%=session.getAttribute("sM")%></div>
            <%}%>
            <%if (session.getAttribute("eM") != null) {%>
            <div class="alert alert-danger"><%=session.getAttribute("eM")%></div>
            <%}%>
            </div>
            <div class="row" style="margin-top: 100px;">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In / Register</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" action="login.jsp" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Username" name="username" type="text" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="password" type="password" required value="">
                                    </div>
                                    <%--<div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                        </label>
                                    </div>--%>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <%if (session.getAttribute("username") == null || !session.getAttribute("username").equals("admin")) {%>
                                    <input type="hidden" name="login" value="true"/>
                                    <input type="submit" class="btn btn-lg btn-success btn-block" value="Login"/>
                                    <%} else {%>
                                    <input type="hidden" name="register" value="true"/>
                                    <input type="submit" class="btn btn-lg btn-primary btn-block" value="Register"/>
                                    <%}%>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="vendor/metisMenu/metisMenu.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="dist/js/sb-admin-2.js"></script>

        <%
            /*} catch (Exception e) {
                System.out.println(e);
            } finally {

                db.close();
            }*/

        %>

    </body>

</html>
