<%-- 
    Document   : downloadExcelPrev
    Created on : Dec 22, 2018, 5:52:41 AM
    Author     : md_al
--%>

<%@page import="com.salesreport.src.Statics"%>
<%@page  contentType="text/html;charset=UTF-8" language="java"%>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.io.*"%>
<%
 if(session.getAttribute("username")==null || 
           (!session.getAttribute("username").equals("admin")&&!session.getAttribute("userId").equals(request.getParameter("userId")))) {
    response.sendRedirect("home.jsp");
   } 
  String filename = request.getParameter("userId")+"f.xlsx";   
  String filepath = Statics.FILE_PATH_PREFIX;   
  
//System.out.println("sfilepath in jsp file is........."+sFile);
 String mimeType = getMimeType(filename);
 //System.out.println(mimeType);
 //response.setContentType(mimeType);
 //response.setHeader("Content-Disposition", "inline;filename=\"" + sFile + "\"");
 response.setContentType(mimeType);
 response.setHeader("Content-disposition","attachment;filename=\""+filename+"\"");
  
 
 FileInputStream in = null;
 OutputStream output=null;
            try {
                output = response.getOutputStream();
                in = new FileInputStream(filepath+filename);
                byte[] buffer = new byte[5 * 1024];
                int size = 0;
                while ((size = in.read(buffer, 0, buffer.length)) > 0) {
                    output.write(buffer, 0, size);
                }
                 
                         
            } catch (Exception e) {
            } 
             
             
            finally {
                in.close();     
                output.flush(); 
                output.close();
            }
            if (true)
                return;
              
 
%>
<%!
static String getMimeType (String fName) {
        fName = fName.toLowerCase();
        if (fName.endsWith(".jpg") || fName.endsWith(".jpeg") || fName.endsWith(".jpe")) return "image/jpeg";
        else if (fName.endsWith(".gif")) return "image/gif";
        else if (fName.endsWith(".pdf")) return "application/pdf";
        else if (fName.endsWith(".htm") || fName.endsWith(".html") || fName.endsWith(".shtml")) return "text/html";
        else if (fName.endsWith(".avi")) return "video/x-msvideo";
        else if (fName.endsWith(".mov") || fName.endsWith(".qt")) return "video/quicktime";
        else if (fName.endsWith(".mpg") || fName.endsWith(".mpeg") || fName.endsWith(".mpe")) return "video/mpeg";
        else if (fName.endsWith(".zip")) return "application/zip";
        else if (fName.endsWith(".tiff") || fName.endsWith(".tif"))  return "image/tif";
        else if (fName.endsWith(".rtf")) return "application/rtf";
        else if (fName.endsWith(".mid") || fName.endsWith(".midi")) return "audio/x-midi";
        else if (fName.endsWith(".xl") || fName.endsWith(".xls") || fName.endsWith(".xlv")
                || fName.endsWith(".xla") || fName.endsWith(".xlb") || fName.endsWith(".xlt")
                || fName.endsWith(".xlm") || fName.endsWith(".xlk")) return "application/vnd.ms-excel";
        else if (fName.endsWith(".doc") || fName.endsWith(".doc")) return "application/msword";
        else if (fName.endsWith(".png")) return "image/png";
        else if (fName.endsWith(".xml")) return "text/xml";
        else if (fName.endsWith(".svg")) return "image/svg+xml";
        else if (fName.endsWith(".mp3")) return "audio/mp3";
        else if (fName.endsWith(".ogg")) return "audio/ogg";
        else return "text/plain";
    }
%>
