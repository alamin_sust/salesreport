<%-- 
    Document   : downloadExcel
    Created on : Dec 22, 2018, 4:52:56 AM
    Author     : md_al
--%>
<%@page import="com.salesreport.src.Statics"%>
<%@ page contentType="application/vnd.ms-excel" pageEncoding="ISO-8859-1"%> 

<%    
   if(session.getAttribute("username")==null || 
           (!session.getAttribute("username").equals("admin")&&!session.getAttribute("userId").equals(request.getParameter("userId")))) {
    response.sendRedirect("home.jsp");
   } 
  String filename = request.getParameter("userId")+"f.xls";   
  String filepath = Statics.FILE_PATH_PREFIX;   
  response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
  //response.setHeader("Content-Disposition", "attachment; filename="+filename);
  response.setHeader("Content-Disposition", "attachment;filename="+filename);
  java.io.FileInputStream fileInputStream=new java.io.FileInputStream(filepath + filename);  
            
  int i;   
  while ((i=fileInputStream.read()) != -1) {  
    out.write(i);   
  }   
  fileInputStream.close();   

%>